# twostep-java 
[![Build Status](https://travis-ci.org/objectia/twostep-java.svg?branch=master)](https://travis-ci.org/objectia/twostep-java)
[![codecov](https://codecov.io/gh/objectia/twostep-java/branch/master/graph/badge.svg)](https://codecov.io/gh/objectia/twostep-java)

Java API client for twostep.io

You can sign up for a twostep account at https://twostep.io.

## Requirements

Java 1.7 or later.

## Installation

### Maven

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>com.objectia</groupId>
  <artifactId>twostep-java</artifactId>
  <version>0.6.1</version>
</dependency>
```

### Gradle

Add this dependency to your project's build file:

```groovy
compile "com.objectia:twostep-java:0.6.1"
```

### Others

You'll need to manually install the following JARs:

* The twostep JAR from https://github.com/objectia/twostep-java/releases/latest
* [Google Gson](https://github.com/google/gson) from <https://repo1.maven.org/maven2/com/google/code/gson/gson/2.8.2/gson-2.8.2.jar>.
* [Java JWT](https://github.com/auth0/java-jwt) from <https://repo1.maven.org/maven2/com/auth0/java-jwt/3.3.0/java-jwt-3.3.0.jar>.

### [ProGuard](http://proguard.sourceforge.net/)

If you're planning on using ProGuard, make sure that you exclude the twostep bindings. You can do this by adding the following to your `proguard.cfg` file:

    -keep class com.objectia.** { *; }

## Documentation

Please see the [Java API docs](https://docs.twostep.io/api/index.html) for the most up-to-date documentation.

## Usage

Example.java

```java
package examples;

import com.objectia.twostep;
import com.objectia.twostep.exceptions.APIException;
import com.objectia.twostep.exceptions.ResponseException;
import com.objectia.twostep.models.User;

public class Example {

    public static void main(String[] args) {

        Client client = new Client();
        
        try {
            User user = client.createUser("jdoe@example.com", "+12125551234", 1);
            // ...
        } catch (ResponseException ex) {
            System.err.println("Response error: " + ex.getMessage());
        } catch (APIException ex) {
            System.err.println("API error: " + ex.getMessage());
        }
    }
}```
