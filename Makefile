.PHONY: test build doc

test:
	mvn test -f ./pom.xml

build:
	mvn compile -f ./pom.xml
	
doc: 
	javadoc -public -doctitle "Twostep Java SDK" -d ./doc ./src/main/java/com/objectia/twostep/*.java ./src/main/java/com/objectia/twostep/exceptions/*.java ./src/main/java/com/objectia/twostep/models/*.java
