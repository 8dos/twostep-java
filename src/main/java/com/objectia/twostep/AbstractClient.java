package com.objectia.twostep;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.objectia.twostep.exceptions.APIConnectionException;
import com.objectia.twostep.exceptions.APIException;
import com.objectia.twostep.exceptions.APITimeoutException;
import com.objectia.twostep.exceptions.ResponseException;
import com.objectia.twostep.models.Error;
import com.objectia.twostep.models.Response;
import com.objectia.twostep.models.Token;
import com.objectia.twostep.models.TokenStore;

/**
 * Abstract REST client base class
 */
public abstract class AbstractClient {

    private static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

    protected String apiUrl = Constants.LIVE_API_URL;
    protected int timeout = Constants.DEFAULT_TIMEOUT;

    protected String clientId;
    protected String clientSecret;
    protected TokenStore tokenStore = null;

    protected AbstractClient(final String clientId, final String clientSecret, final boolean sandbox) throws IllegalArgumentException {
        this.clientId = clientId;
        this.clientSecret = clientSecret;

        if (this.clientId == null || this.clientId.isEmpty()) {
            throw new IllegalArgumentException("No Client ID provided");
        }
        if (this.clientSecret == null || this.clientSecret.isEmpty()) {
            throw new IllegalArgumentException("No Client secret provided");
        }

        this.apiUrl = sandbox ? Constants.SANDBOX_API_URL : Constants.LIVE_API_URL;
    }

    protected AbstractClient(final String clientId, final String clientSecret) throws IllegalArgumentException {
        this(clientId, clientSecret, false);
    }


    //******************************************************

    /**
     * Sets the timeout value for the REST client.
     * 
     * @param timeout is the timeout in seconds. Must be within 1 to 180 seconds.
     */
    public void setTimeout(final int timeout) {
        if (timeout < 1 || timeout > 180) {
            throw new IllegalArgumentException("Timeout must be within 1 and 180 seconds");
        }
        this.timeout = timeout;
    }

    /**
     * Gets the timeout value.
     * 
     * @return the timeout in seconds
     */
    public int getTimeout() {
        return this.timeout;
    }

    /**
     * Sets the base API URL to be used when sending requests from the client.
     * 
     * @param url is the API URL to be used, ie. https://api.twostep.io
     */
    public void setApiUrl(final String url) {
        this.apiUrl = url;
    }

    /**
     * Gets the active base API URL.
     * 
     * @return a string with the API URL 
     */
    public String getApiUrl() {
        return this.apiUrl;
    }

    /**
     * Sets the token store to be used byu the client
     * 
     * <p>The token store is used by the client to store the access token between requests</p>
     * 
     * @param ts a valid token store object
     */
    public void setTokenStore(final TokenStore ts) {
        this.tokenStore = ts;
    }

    /**
     * Returns the active token store
     * 
     * @return a token store object
     */
    public TokenStore getTokenStore() {
        return this.tokenStore;
    }

    //******************************************************

    protected Response get(final String path) throws APIException {
        return requestWithToken("GET", path, null);
    }

    protected Response post(final String path, final Object params) throws APIException {
        return requestWithToken("POST", path, params);
    }

    protected Response put(final String path, final Object params) throws APIException {
        return requestWithToken("PUT", path, params);
    }

    protected Response delete(final String path) throws APIException {
        return requestWithToken("DELETE", path, null);
    }

    //******************************************************

    protected Token getToken() throws APIException {
        Token token = null;

        // Try get from token store
        TokenStore store = this.getTokenStore();
        if (store != null) {
            token = store.load();
        }

        if (token != null && token.isValid()) {
            if (token.isExpired()) {
                // Use existing refresh_token to get a new token
                return refreshToken(token);
            }
            return token;
        }

        // Get a new token from server
        return newToken();
    }

    protected Token newToken() throws APIException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("client_id", this.clientId);
        params.put("client_secret", this.clientSecret);
        params.put("grant_type", "client_credentials");

        Response resp = request("POST", "/auth/token", params, null);
        Token token = Token.fromJSON(resp.getBody());

        TokenStore store = this.getTokenStore();
        if (store != null) {
            store.save(token);
        }

        return token;
    }

    protected Token refreshToken(final Token oldToken) throws APIException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("client_id", this.clientId);
        params.put("client_secret", this.clientSecret);
        params.put("grant_type", "refresh_token");
        params.put("refresh_token", oldToken.getRefreshToken());

        Response resp = request("POST", "/auth/token", params, null);
        Token token = Token.fromJSON(resp.getBody());

        TokenStore store = this.getTokenStore();
        if (store != null) {
            store.save(token);
        }

        return token;
    }

    //***************************************************************

    protected Response requestWithToken(final String method, final String path, final Object payload)
            throws APIException {
        Token token = getToken();
        if (token == null) {
            throw new ResponseException(400, "Unable to retrieve access token");
        }
        return request(method, path, payload, token);
    }

    protected Response request(final String method, final String path, final Object payload, final Token token)
            throws APIException {
        String uri = this.apiUrl + path;

        if (uri.startsWith("https://")) {
            return secureRequest(method, path, payload, token);
        }

        HttpURLConnection conn = null;
        try {
            URL url = new URL(uri);

            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod(method);
            if (token != null) {
                conn.setRequestProperty("Authorization", "Bearer " + token.getAccessToken());
            }
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("User-Agent", Constants.USER_AGENT);

            conn.setConnectTimeout(this.timeout * 1000);
            conn.setReadTimeout(this.timeout * 2000);
            conn.setUseCaches(false);

            if (payload != null) {
                // Add payload
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);
                String json = GSON.toJson(payload);
                byte buffer[] = json.getBytes("UTF-8");
                conn.setFixedLengthStreamingMode(buffer.length);
                OutputStream os = conn.getOutputStream();
                os.write(buffer);
                os.flush();
                os.close();
            }

            String body = null;
            int statusCode = conn.getResponseCode();
            if (statusCode >= 200 && statusCode < 300) {
                body = getBody(conn.getInputStream());
            } else {
                body = getBody(conn.getErrorStream());
                Error err = GSON.fromJson(body, Error.class);
                //LOGGER.warning(err.getMessage());
                throw new ResponseException(err.getStatus(), err.getMessage());
            }

            return new Response(statusCode, body, conn.getHeaderFields());
        } catch (java.net.SocketTimeoutException e) {
            throw new APITimeoutException("The request timed out");
        } catch (IOException ex) {
            //LOGGER.severe("Unable to connect to server");
            throw new APIConnectionException(
                    "Unable to connect to server. Please check your internet connection and try again.");
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    protected Response secureRequest(final String method, final String path, final Object payload, final Token token)
            throws APIException {
        String uri = this.apiUrl + path;

        HttpsURLConnection conn = null;
        try {
            URL url = new URL(uri);

            conn = (HttpsURLConnection) url.openConnection();

            conn.setRequestMethod(method);
            if (token != null) {
                conn.setRequestProperty("Authorization", "Bearer " + token.getAccessToken());
            }
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("User-Agent", Constants.USER_AGENT);

            conn.setConnectTimeout(this.timeout * 1000);
            conn.setReadTimeout(this.timeout * 2000);
            conn.setUseCaches(false);

            if (payload != null) {
                // Add payload
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);
                String json = GSON.toJson(payload);
                byte buffer[] = json.getBytes("UTF-8");
                conn.setFixedLengthStreamingMode(buffer.length);
                OutputStream os = conn.getOutputStream();
                os.write(buffer);
                os.flush();
                os.close();
            }

            String body = null;
            int statusCode = conn.getResponseCode();
            if (statusCode >= 200 && statusCode < 300) {
                body = getBody(conn.getInputStream());
            } else {
                body = getBody(conn.getErrorStream());
                Error err = GSON.fromJson(body, Error.class);
                //LOGGER.warning(err.getMessage());
                throw new ResponseException(err.getStatus(), err.getMessage());
            }

            return new Response(statusCode, body, conn.getHeaderFields());
        } catch (java.net.SocketTimeoutException e) {
            throw new APITimeoutException("The request timed out");
        } catch (IOException ex) {
            //LOGGER.severe("Unable to connect to server");
            throw new APIConnectionException(
                    "Unable to connect to server. Please check your internet connection and try again.");
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    protected String getBody(final InputStream is) throws IOException {
        //\A is the beginning of the stream boundary
        Scanner scanner = new Scanner(is, "UTF-8");
        scanner.useDelimiter("\\A");
        String body = scanner.next();
        scanner.close();
        is.close();
        return body;
    }

    protected String encode(final String str) throws IllegalArgumentException{
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (Exception ex ) {
            throw new IllegalArgumentException("Failed to url encode argment: " + str);
        }
    }
}
