package com.objectia.twostep;

/**
 * Constants
 */
public final class Constants {

    private Constants() {}

    public static final String VERSION = "0.6.1";

    public static final String USER_AGENT = "twostep-java/" + VERSION;
    
    public static final String LIVE_API_URL = "https://api.twostep.io";
    
    public static final String SANDBOX_API_URL = "http://localhost:4000"; // "https://sandbox-api.twostep.io";
    
    public static final int DEFAULT_TIMEOUT = 30; // in seconds
}