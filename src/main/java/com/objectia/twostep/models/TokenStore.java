package com.objectia.twostep.models;

/**
 * TokenStore interface.
 */
public interface TokenStore {
    /**
     * Loads a token from the store. 
     * 
     * @return a valid access token object, or null if the token store was empty
     */
    Token load();

    /**
     * Saves a token
     * 
     * @param token the token to be stored
     */
    void save(Token token);

    /**
     * Clears the token store
     */
    void clear();
}