package com.objectia.twostep.models;

import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Access token
 */
public class Token {
    private static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
    
    @SerializedName("access_token")
    private final String accessToken;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("scope")
    private String scope;

    @SerializedName("expires_in")
    private int expiresIn;

    @SerializedName("refresh_token")
    private String refreshToken;

    /**
     * Class constructor
     */
    public Token() {
        this.accessToken = null;
        this.tokenType = null;
        this.scope = null;
        this.expiresIn = 0;
        this.refreshToken = null;
    }

    /**
     * Class constructor
     * 
     * @param access a JSON Web Token (JWT)
     * @param typ the access token type. Must be "Bearer"
     * @param scope the scope
     * @param exp the expire time in seconds, default is 3600 seconds = 1 hour
     * @param refresh the refresh token (a UUID)
     */
    public Token(String access, String typ, String scope, int exp, String refresh) {
        this.accessToken = access;
        this.tokenType = typ;
        this.scope = scope;
        this.expiresIn = exp;
        this.refreshToken = refresh;
    }

    /**
     * Gets the access token
     * 
     * @return a string with the JWT access token
     */
    public String getAccessToken() {
        return this.accessToken;
    }

    /**
     * Gets the token type
     * 
     * @return a string with the token type
     */
    public String getTokenType() {
        return this.tokenType;
    }

    /**
     * Gets the scope
     * 
     * @return a string with the defined scope
     */
    public String getScope() {
        return this.scope;
    }

    /**
     * Gets the expired time
     * 
     * @return a numeric expire time in seconds
     */
    public int getExpiresIn() {
        return this.expiresIn;
    }

    /**
     * Gets the refresh token
     * 
     * @return a string containing the refresh token in UUID format
     */
    public String getRefreshToken() {
        return this.refreshToken;
    }

    /**
     * Checks if token is valid.
     * 
     * @return true if token is valid, false otherwise
     */
    public boolean isValid() {
        if (this.accessToken == null || this.accessToken.isEmpty()) {
            return false;
        }
        if (this.refreshToken == null || this.refreshToken.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
       * Checks if access_token has expired.
       * 
       * @return true if token has expired, false otherwise
       */
    public boolean isExpired() {
        try {
            DecodedJWT jwt = JWT.decode(this.accessToken);

            Date now = new Date();
            Date expiresAt = jwt.getExpiresAt();
            if (now.before(expiresAt)) {
                // Token is still valid
                return false;
            }
        } catch (JWTDecodeException exception) {
            // Invalid token
        }

        return true; // invalid or expired
    }

    /**
     * Creates a Token from a JSON string
     * 
     * @param json a string containing valid JSON
     * @return a Token object
     */
    public static Token fromJSON(final String json) {
        return GSON.fromJson(json, Token.class);
    }

    /**
     * Creates a JSON string from the Token object.
     * 
     * @return a JSON data string
     */
    public String toJSON() {
        return GSON.toJson(this);
    }
}