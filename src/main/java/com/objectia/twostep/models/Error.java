package com.objectia.twostep.models;

/**
 * Error response
 */
public class Error {
    private final int status;
    private final String message;
    private final boolean success;

    /**
     * Class constructor
     */
    public Error() {
        this.status = 0;
        this.message = null;
        this.success = false;
    }

    /**
     * Gets the status code
     * 
     * @return a numeric HTTP status code
     */
    public int getStatus() {
        return this.status;
    }

    /**
     * Gets the error message
     * 
     * @return a string with the error message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Gets the success flag
     * 
     * @return true if successful, otherwise false
     */
    public boolean getSuccess() {
        return this.success;
    }
}
