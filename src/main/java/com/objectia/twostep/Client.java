package com.objectia.twostep;

import java.util.HashMap;
import java.util.Map;

import com.objectia.twostep.exceptions.APIException;
import com.objectia.twostep.models.Call;
import com.objectia.twostep.models.Response;
import com.objectia.twostep.models.Sms;
import com.objectia.twostep.models.User;

/**
 * Java API client for twostep.io.
 */
public final class Client extends AbstractClient {

    /** 
    * Class constructor.
    * 
    * @param clientId the client id to be used when accessing the API
    * @param clientSecret the client secret for your account.
    * @param sandbox use the sandbox api url if true, otherwise use live api url.
    */
    public Client(final String clientId, final String clientSecret, final boolean sandbox) throws IllegalArgumentException {
        super(clientId, clientSecret, sandbox);
    }

    /** 
    * Class constructor.
    * 
    * @param clientId the client id to be used when accessing the API
    * @param clientSecret the client secret for your account.
    */
    public Client(final String clientId, final String clientSecret) throws IllegalArgumentException {
        super(clientId, clientSecret);
    }

    /**
     * Creates a new user.
     * 
     * @param email a valid email address
     * @param phone a valid international phone number
     * @param countryCode a numeric country code
     * @param sendInstallLink a flag that indicates that an install link should be sent to the user
     * @return the User created
     */
    public User createUser(final String email, final String phone, final int countryCode, final boolean sendInstallLink) throws APIException, IllegalArgumentException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", email);
        params.put("phone", phone);
        params.put("country_code", countryCode);
        params.put("send_install_link", sendInstallLink);
        Response resp = post("/v1/users", params);
        return User.fromJSON(resp.getBody());
    }

    /**
     * Creates a new user.
     * 
     * @param email a valid email address
     * @param phone a valid international phone number
     * @param countryCode a numeric country code
     * @return the User created
     */
    public User createUser(final String email, final String phone, final int countryCode) throws APIException, IllegalArgumentException {
        return createUser(email, phone, countryCode, false);
    }

    /**
     * Get user by user id.
     * 
     * @param userId the id of the your to be retrieved
     * @return the User for the given id
     */
    public User getUser(final String userId) throws APIException, IllegalArgumentException {
        Response resp = get("/v1/users/" + encode(userId));
        return User.fromJSON(resp.getBody());
    }

    /**
     * Remove user by user id.
     * 
     * @param userId the id of the your to be deleted
     * @return the User the given id
     */
    public User removeUser(final String userId) throws APIException, IllegalArgumentException { 
        Response resp = delete("/v1/users/" + encode(userId));
        return User.fromJSON(resp.getBody());
    }
    
    /**
     * Request sms message to be sent to user.
     * 
     * @param userId the id of the your to be messaged
     * @param force a flag indicating if a sms should be sent even if an app has been registered for this user
     * @return an Sms receipt
     */
    public Sms requestSms(final String userId, final boolean force) throws APIException, IllegalArgumentException  {   
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("force", force);
        Response resp = post("/v1/users/" + encode(userId) + "/sms", params);
        return Sms.fromJSON(resp.getBody());
    }

    /**
     * Request sms message to be sent to user.
     * 
     * @param userId the id of the your to be messaged
     * @return an Sms receipt
     */
    public Sms requestSms(final String userId) throws APIException, IllegalArgumentException  {   
        return requestSms(userId, false);
    }

    /**
     * Request voice call to be sent to user.
     * 
     * @param userId the id of the your to be called
     * @param force a flag indicating if a call should be started even if an app has been registered for this user
     * @return a Call receipt
     */
    public Call requestCall(final String userId, final boolean force) throws APIException, IllegalArgumentException {  
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("force", force);
        Response resp = post("/v1/users/" + encode(userId) + "/call", params);
        return Call.fromJSON(resp.getBody());
    }

    /**
     * Request voice call to be sent to user.
     * 
     * @param userId the id of the your to be called
     * @return a Call receipt
     */
    public Call requestCall(final String userId) throws APIException, IllegalArgumentException  {   
        return requestCall(userId, false);
    }
}
