package com.objectia.twostep;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.objectia.twostep.exceptions.APIConnectionException;
import com.objectia.twostep.exceptions.APIException;
import com.objectia.twostep.exceptions.ResponseException;
import com.objectia.twostep.models.Call;
import com.objectia.twostep.models.Entity;
import com.objectia.twostep.models.Error;
import com.objectia.twostep.models.Headers;
import com.objectia.twostep.models.Response;
import com.objectia.twostep.models.Sms;
import com.objectia.twostep.models.Token;
import com.objectia.twostep.models.User;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TwostepTest {

    private static Client client = null;

    private static String CLIENT_ID = "12345";
    private static String CLIENT_SECRET = "67890";

    @Test
    public void testWithNoClientId() throws APIException {
        try {
            new Client(null, CLIENT_SECRET);
            fail();

        } catch (IllegalArgumentException ex) {
            assertNotNull(ex);
        }
        try {
            new Client("", CLIENT_SECRET);
            fail();

        } catch (IllegalArgumentException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testWithNoClientSecret() throws APIException {
        try {
            new Client(CLIENT_ID, null);
            fail();

        } catch (IllegalArgumentException ex) {
            assertNotNull(ex);
        }
        try {
            new Client(CLIENT_ID, "");
            fail();

        } catch (IllegalArgumentException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testTimeout() throws APIException {
        Client cli = new Client(CLIENT_ID, CLIENT_SECRET);
        cli.setTimeout(10);
        assertEquals(10, cli.getTimeout());

        try {
            cli.setTimeout(0);
            fail();

        } catch (IllegalArgumentException ex) {
            assertNotNull(ex);
        }

        try {
            cli.setTimeout(1000);
            fail();

        } catch (IllegalArgumentException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testApiUrl() throws APIException {
        Client cli = new Client(CLIENT_ID, CLIENT_SECRET);
        cli.setApiUrl("http://localhost");
        assertEquals("http://localhost", cli.getApiUrl());
    }

    @Test
    public void testGetUser() throws APIException {
        User user = client.getUser("123456");
        assertNotNull(user);
    }

    @Test
    public void testGetUnknownUser() throws APIException {
        try {
            client.getUser("000000");
            fail();

        } catch (ResponseException ex) {
            assertNotNull(ex);
            assertNotEquals(0, ex.getStatus());
        }
    }

    @Test
    public void testGetUserWithExpiredToken() throws APIException {
        Client cli = new Client(CLIENT_ID, CLIENT_SECRET, true);
        cli.setTokenStore(new ExpiredTokenStore());
        User user = cli.getUser("123456");
        assertNotNull(user);
    }

    @Test
    public void testGetUserWithBadToken() throws APIException {
        Client cli = new Client(CLIENT_ID, CLIENT_SECRET, true);
        cli.setTokenStore(new BadTokenStore());
        User user = cli.getUser("123456");
        assertNotNull(user);
    }

    @Test
    public void testApiWhenServerIsDown() throws APIException {
        try {
            Client cli = new Client(CLIENT_ID, CLIENT_SECRET);
            cli.setApiUrl("http://localhost:4444");
            cli.getUser("123456");
            fail();
        } catch (APIConnectionException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testApiWhenUsingFakeServer() throws APIException {
        try {
            Client cli = new Client(CLIENT_ID, CLIENT_SECRET);
            cli.setApiUrl("http://fakeserver:4000");
            cli.getUser("123456");
            fail();
        } catch (APIConnectionException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testCreateUser() throws APIException {
        User user = client.createUser("jdoe@example.com", "+12125551234", 1);
        assertNotNull(user);
    }

    @Test
    public void testCreateUserWithLink() throws APIException {
        User user = client.createUser("jdoe@example.com", "+12125551234", 1, true);
        assertNotNull(user);
    }

    @Test
    public void testCreateUserWithInvalidEmail() throws APIException {
        try {
            client.createUser("jdoeexample.com", "+12125551234", 1);
            fail();
        } catch (ResponseException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testRemoveUser() throws APIException {
        User user = client.removeUser("123456");
        assertNotNull(user);
    }

    @Test
    public void testRemoveUnknownUser() throws APIException {
        try {
            client.removeUser("000000");
            fail();

        } catch (ResponseException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testRequestSms() throws APIException {
        Sms receipt = client.requestSms("123456");
        assertNotNull(receipt);
        @SuppressWarnings("unused") boolean ignored = receipt.isIgnored();
        @SuppressWarnings("unused") String phone = receipt.getPhone();
    }

    @Test
    public void testRequestSmsForce() throws APIException {
        Sms receipt = client.requestSms("123456", true);
        assertNotNull(receipt);
    }

    @Test
    public void testRequestSmsUnknown() throws APIException {
        try {
            client.requestSms("000000");
            fail();

        } catch (ResponseException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testRequestCall() throws APIException {
        Call receipt = client.requestCall("123456");
        assertNotNull(receipt);
        @SuppressWarnings("unused") boolean ignored = receipt.isIgnored();
        @SuppressWarnings("unused") String phone = receipt.getPhone();
    }

    @Test
    public void testRequestCallForce() throws APIException {
        Call receipt = client.requestCall("123456", true);
        assertNotNull(receipt);
    }

    @Test
    public void testRequestCallUnknown() throws APIException {
        try {
            client.requestCall("000000");
            fail();

        } catch (ResponseException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testEntity() {
        Entity<User> entity = new Entity<User>();
        assertEquals(0, entity.getStatus());       
        assertNull(entity.getMessage());
        assertEquals(false, entity.getSuccess());
    }

    @Test
    public void testError() {
        Error err = new Error();
        assertEquals(0, err.getStatus());       
        assertNull(err.getMessage());
        assertEquals(false, err.getSuccess());
    }

    @Test
    public void testResponse() {
        Response resp = new Response(400, "body");
        assertEquals(400, resp.getStatus());       
        assertEquals("body", resp.getBody());
        assertNull(resp.getHeaders());
    }

    @Test
    public void testHeaders() {
        Map<String, List<String>> headers = new HashMap<String, List<String>>();

        // Null headers
        Headers h1 = new Headers(null);
        assertNull(h1.get("?"));       
        assertNull(h1.values("?"));       

        // Empty headers
        Headers h2 = new Headers(headers);
        assertNull(h2.get("?"));       
        assertNull(h2.values("?"));       

        ArrayList<String> a = new ArrayList<String>();
        a.add("a");
        a.add("b");
        headers.put("h", a);
        Headers h3 = new Headers(headers);
        assertNotNull(h3.get("h"));       
        assertNotNull(h3.values("h"));       
    }

    @Test
    public void testUser() {

        String json = "{ \"status\": 200, \"data\": {" +
        " \"id\": \"ID\", " +
        " \"email\": \"EMAIL\", " +
        " \"phone\": \"PHONE\", " +
        " \"country_code\": 1, " +
        " \"registered\": true, " +
        " \"confirmed\": false " +
        " } }";

        User u = User.fromJSON(json);
        assertEquals("ID", u.getId());
        assertEquals("EMAIL", u.getEmail());
        assertEquals("PHONE", u.getPhone());
        assertEquals(1, u.getCountryCode());
        assertEquals(true, u.isRegistered());
        assertEquals(false, u.isConfirmed());

        assertNotNull(u.toJSON());
    }

    @Test
    public void testToken() {

        Token t = new Token("access", "type", "scope", 1, "refresh");
        assertEquals("access", t.getAccessToken());
        assertEquals("type", t.getTokenType());
        assertEquals("scope", t.getScope());
        assertEquals(1, t.getExpiresIn());
        assertEquals("refresh", t.getRefreshToken());

        assertNotNull(t.toJSON());

        Token t2 = new Token(null, "type", "scope", 1, "refresh");
        assertEquals(false, t2.isValid());

        Token t3 = new Token("", "type", "scope", 1, "refresh");
        assertEquals(false, t3.isValid());

        Token t4 = new Token("access", "type", "scope", 1, null);
        assertEquals(false, t4.isValid());

        Token t5 = new Token("access", "type", "scope", 1, "");
        assertEquals(false, t5.isValid());

        Token t6 = new Token("access", "type", "scope", 1, "refresh");
        assertEquals(true, t6.isExpired());
        
        Token t7 = new Token("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A", "Bearer", "none", 3600, "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f");
        assertEquals(true, t7.isExpired());
    }


    //**************************************************************************

    @BeforeClass
    public static void setUp() {
        client = new Client(CLIENT_ID, CLIENT_SECRET, true);
        client.setTokenStore(new MyTokenStore());
    }

    @AfterClass
    public static void tearDown() {

    }
}