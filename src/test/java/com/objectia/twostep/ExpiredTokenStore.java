package com.objectia.twostep;

import com.objectia.twostep.models.Token;
import com.objectia.twostep.models.TokenStore;

public class ExpiredTokenStore implements TokenStore {

  public Token load() {
    return new Token("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A", "Bearer", "none", 3600, "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f");
  } 

  public void save(Token token) {
  } 

  public void clear() {
  }

}

