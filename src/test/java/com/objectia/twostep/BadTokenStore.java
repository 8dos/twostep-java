package com.objectia.twostep;

import com.objectia.twostep.models.Token;
import com.objectia.twostep.models.TokenStore;

public class BadTokenStore implements TokenStore {

  public Token load() {
    return new Token("this.is.bad", "Bearer", "none", 3600, "");
  } 

  public void save(Token token) {
  } 

  public void clear() {
  }

}

